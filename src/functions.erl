%%%-------------------------------------------------------------------
%%% @author tjakopan
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Mar 2019 16:03
%%%-------------------------------------------------------------------
-module(functions).
-author("tjakopan").

%% API
%%-export([]).
-compile(export_all).

greet(male, Name) -> io:format("Hello, Mr. ~s!", [Name]);
greet(female, Name) -> io:format("Hello, Mrs. ~s!", [Name]);
greet(_, Name) -> io:format("Hello, ~s!", [Name]).

head([H | _]) -> H.

second([_, X | _]) -> X.

same(X, X) -> true;
same(_, _) -> false.

valid_time({Date = {Y, M, D}, Time = {H, Min, S}}) ->
  io:format("The Date tuple (~p) says today is: ~p/~p/~p,~n", [Date, Y, M, D]),
  io:format("The Time tuple (~p) inidicates: ~p:~p:~p.~n", [Time, H, Min, S]);
valid_time(_) -> io:format("Stop feeding me wrong data!~n").
