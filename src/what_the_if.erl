%%%-------------------------------------------------------------------
%%% @author tjakopan
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Mar 2019 17:21
%%%-------------------------------------------------------------------
-module(what_the_if).
-author("tjakopan").

%% API
%%-export([heh_fine/0, oh_god/1, help_me/1]).
-compile(export_all).

heh_fine() ->
  if 1 =:= 1 ->
    works
  end,
  if 1 =:= 2; 1 =:=1 ->
    works
  end,
  if 1 =:= 2, 1 =:= 1 ->
    fails
  end.

oh_god(N) ->
  if N =:= 2 -> might_succeed;
    true -> always_does %% This is Erlang's if's 'else'!
  end.

%% Note, this one would be better as a pattern match in function heads!
%% I'm doing it this way for the sake of the example.
help_me(Animal) ->
  Talk = if Animal == cat -> "meow";
           Animal == beef -> "mooo";
           Animal == dog -> "bark";
           Animal == tree -> "bark";
           true -> "fgdadfgna"
         end,
  {Animal, "says " ++ Talk ++ "!"}.
