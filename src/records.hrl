%%%-------------------------------------------------------------------
%%% @author tjakopan
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. Oct 2020 19:25
%%%-------------------------------------------------------------------
-author("tjakopan").

-record(included, {
  some_field,
  some_default = "yeah!",
  unimaginative_name
}).
