%%%-------------------------------------------------------------------
%%% @author tjakopan
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. Mar 2019 07:40
%%%-------------------------------------------------------------------
-module(cases).
-author("tjakopan").

%% API
%%-export([insert/2, beach/1, beachf/1]).
-compile(export_all).

insert(X, []) -> [X];
insert(X, Set) ->
  case lists:member(X, Set) of
    true -> Set;
    false -> [X | Set]
  end.

beach(Temperature) ->
  case Temperature of
    {celsius, N} when N >= 20, N =< 45 -> 'favorable';
    {kelvin, N} when N >= 293, N =< 318 -> 'scientifically favorable';
    {fahrenheit, N} when N >= 68, N =< 113 -> 'favorable in the US';
    _ -> 'avoid beach'
  end.

beachf({celsius, N}) when N >= 20, N =< 45 -> 'favorable';
beachf({kelvin, N}) when N >= 293, N =< 318 -> 'scientifically favorable';
beachf({fahrenheit, N}) when N >= 68, N =< 113 -> 'favorable in the US';
beachf(_) -> 'avoid beach'.
