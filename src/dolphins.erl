%%%-------------------------------------------------------------------
%%% @author tjakopan
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Oct 2020 13:40
%%%-------------------------------------------------------------------
-module(dolphins).
-author("tjakopan").

%% API
%%-export([]).
-compile(export_all).

dolphin1() ->
  receive
    do_a_flip -> io:format("How about no?~n");
    fish -> io:format("So long and thanks for all the fish!~n");
    _ -> io:format("Heh, we're smarter than you humans.~n")
  end.

dolphin2() ->
  receive
    {From, do_a_flip} -> From ! "How about no?";
    {From, fish} -> From ! "So long and thanks for all the fish!";
    _ -> io:format("Heh, we're smarter than you humans.~n")
  end.

dolphin3() ->
  receive
    {From, do_a_flip} ->
      From ! "How about no?",
      dolphin3();
    {From, fish} ->
      From ! "So long and thanks for all the fish!";
    _ ->
      io:format("Heh, we're smarter than you humans.~n"),
      dolphin3()
  end.
