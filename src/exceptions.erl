%%%-------------------------------------------------------------------
%%% @author tjako
%%% @copyright (C) 2019, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 17. Mar 2019 21:18
%%%-------------------------------------------------------------------
-module(exceptions).
-author("tjakopan").
-compile(export_all).

%% API
-export([]).

throws(F) ->
  try F() of
    _ -> ok
  catch
    Throw -> {throw, caught, Throw}
  end.

errors(F) ->
  try F() of
    _ -> ok
  catch
    error:Error -> {error, caught, Error}
  end.

exits(F) ->
  try F() of
    _ -> ok
  catch
    exit:Exit -> {exit, caught, Exit}
  end.

sword(1) -> throw(slice);
sword(2) -> erlang:error(cut_arm);
sword(3) -> exit(cut_leg);
sword(4) -> throw(punch);
sword(5) -> exit(cross_bridge).

black_knight(Attack) when is_function(Attack, 0) ->
  try Attack() of
    _ -> "None shall pass."
  catch
    throw:slice -> "It is but a scratch.";
    error:cut_arm -> "I've had worse.";
    exit:cut_leg -> "Come on you pansy!";
    _:_ -> "Just a flesh wound."
  end.

talk() -> "blah blah".

whoa() ->
  try
    talk(),
    _Knight = "None shall pass!",
    _Doubles = [N * 2 || N <- lists:seq(1, 100)],
    throw(up),
    _WillReturnThis = tequila
  of
    tequila -> "Hey this worked!"
  catch
      Exception:Reason -> {caught, Exception, Reason}
  end.

im_impressed() ->
  try
    talk(),
    _Knight = "None shall pass!",
    _Doubles = [N * 2 || N <- lists:seq(1, 100)],
    throw(up),
    _WillReturnThis = tequila
  catch
    Exception:Reason -> {caught, Exception, Reason}
  end.

catcher(X, Y) ->
  case catch X / Y of
    {'EXIT', {badarith, _}} -> "uh oh";
    N -> N
  end.

one_or_two(1) -> return;
one_or_two(2) -> throw(return).

%% Look for given value 'Val' in the tree.
has_value(Val, Tree) ->
  try has_value1(Val, Tree) of
    false -> false
  catch
    true -> true
  end.

has_value1(_, {node, nil}) -> false;
has_value1(Val, {node, {_, Val, _, _}}) -> throw(true);
has_value1(Val, {node, {_, _, Left, Right}}) ->
  has_value1(Val, Left),
  has_value1(Val, Right).
